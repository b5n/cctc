#!/bin/bash

# installs a cross compiler toolchain in <CCTCDIR>/gcc-<VERSION> on debian

# accepts four optional arguments

# -n, --nuke
#   rm cctc dir
#   deafult: false

# -c, --clean
#   rm build artifacts
#   default: false

# -d=<VALUE>, --cctc=<VALUE>
#   cctc dir
#   default: $HOME/cctc

# -t=<VALUE>, --target=<VALUE>
#   target triplet
#   default: riscv64-unknown-elf

die() {
    echo "$1"
    exit "$2"
}

prompt() {
    local msg="$1"
    local response=Y
    read -rp "$msg (Y/n): " response

    if [[ ! "$response" =~ ^[YyNn]$ ]]
    then
        prompt "$msg"
    fi

    echo "$response"
}

parse_args() {
    getopt --test
    [[ "$?" -eq 4 ]] || die 'getopt not supported' 1

    local lopts='nuke,clean,cctc:,target:'
    local sopts='ncd:t:'

    local parsed
    if ! parsed=$(getopt -o "$sopts" -l "$lopts" -n "$0" -- "$@")
    then
        die 'getopt failed' 2
    fi

    eval set -- "$parsed"

    local clean=0
    local nuke=0
    local cctc_dir="$HOME/cctc"
    local target=riscv64-unknown-elf
    while true; do
        case "$1" in
            -n|--nuke)
		nuke=1
		shift
		;;
	    -c|--clean)
		clean=1
		shift
		;;
	    -d|--cctc)
		cctc_dir="$2"
		shift 2
		;;
            -t|--target)
		target="$2"
		shift 2
		;;
	    --)
		shift
		break
		;;
	    *)
		die 'unrecognized option' 3
                ;;
	esac
    done

    echo "$nuke $clean $cctc_dir $target"
}

clean_src() {
    local nuke="$1"
    local clean="$2"
    local cctc_dir="$3"

    if [[ "$nuke" -ne 0 ]]
    then
        echo "removing $cctc_dir"
        rm -rf "$cctc_dir"
    elif [[ "$clean" -ne 0 ]]
    then
        echo 'removing build artifacts'
        rm -rf "$cctc_dir/build" "$cctc_dir"/gcc-*
    fi
}

get_src() {
    local cctc_dir="$1"

    local ld_version
    ld_version=$(ld -v | awk '{print $7}')

    local gdb_version
    gdb_version=$(gdb -v \
                      | awk 'NR==1 {version=$NF}
			     END {sub(/\.[0-9]*\.[0-9]*-git$/, "", version);
				  print version}')

    mkdir -p "$cctc_dir"

    if [[ -d "$cctc_dir/binutils" ]]
    then
	echo 'binutils source OK!'
    else
        echo 'retrieving binutils'
	curl -s -o "$cctc_dir/binutils.tar.bz2" \
             https://ftp.gnu.org/gnu/binutils/binutils-"$ld_version".tar.bz2

	tar xjf "$cctc_dir/binutils.tar.bz2" -C "$cctc_dir"
	mv "$cctc_dir/binutils-$ld_version" "$cctc_dir/binutils"
	rm "$cctc_dir/binutils.tar.bz2"
    fi

    if [[ -d "$cctc_dir/gdb" ]]
    then
	echo 'gdb source OK!'
    else
        echo 'retrieving gdb'
	curl -s -o "$cctc_dir/gdb.tar.xz" \
             https://ftp.gnu.org/gnu/gdb/gdb-"$gdb_version".tar.xz

	tar xf "$cctc_dir/gdb.tar.xz" -C "$cctc_dir"
	mv "$cctc_dir/gdb-$gdb_version" "$cctc_dir/gdb"
	rm "$cctc_dir/gdb.tar.xz"
    fi

    if [[ -d "$cctc_dir/gcc" ]]
    then
        git fetch origin
        echo 'gcc source OK!'
    else
        echo 'cloning gcc'
	git clone git://gcc.gnu.org/git/gcc.git "$cctc_dir/gcc"
    fi
}

check_deps() {
    local missing=0
    local deps=(
	bison
	build-essential
	flex
	gdb
	git
	libgmp3-dev
	libisl-dev
	libmpc-dev
	libmpfr-dev
	texinfo
    )

    echo 'verifying dependencies'
    for dep in "${deps[@]}"
    do
	apt list "$dep" 2> /dev/null | grep installed &> /dev/null \
            || { echo "missing dependency: $dep" && missing=$((missing+1)); }
    done

    if [[ missing -gt 0 ]]
    then
	die 'missing dependencies' 1
    else
	echo 'dependencies OK!'
    fi
}

build_src() {
    local cctc_dir="$1"
    local target="$2"
    local gcc_version
    gcc_version=$(gcc --version | awk 'NR==1 {version=$4}
				       END {
					   gsub(/\.[0-9]$/,".0",version);
					   print version}')

    local prefix="$cctc_dir/gcc-$gcc_version"

    if [[ -d "$prefix" ]]
    then
        local response
        response=$(prompt "overwrite $prefix?")
        if [[ "$response" =~ [Nn] ]]
        then
            die 'operation aborted' 0
        fi
    fi

    clean_src 0 1 "$cctc_dir"
    export PATH="$prefix/bin:$PATH"
    mkdir -p "$cctc_dir/build/"{binutils,gdb,gcc}

    cd "$cctc_dir/build/binutils" || die 'binutils build directory not found' 1
    ../../binutils/configure --target="$target" \
			     --prefix="$prefix" \
			     --with-sysroot \
			     --disable-nls \
			     --disable-werror

    make -j"$(nproc)"
    make install

    cd "$cctc_dir/build/gdb" || die 'gdb build directory not found' 1
    ../../gdb/configure --target="$target" \
			--prefix="$prefix" \
                        --with-sysroot \
                        --disable-nls \
			--disable-werror \
                        --enable-languages=c \
                        --without-headers

    make -j"$(nproc)" all-gdb
    make install-gdb

    cd "$cctc_dir/gcc" || die 'gcc source directory not found' 1
    git checkout releases/gcc-"$gcc_version"

    cd "$cctc_dir/build/gcc" || die 'gcc build directory not found' 1
    ../../gcc/configure --target="$target" \
			--prefix="$prefix" \
			--disable-nls \
			--enable-languages=c \
			--without-headers

    make -j"$(nproc)" all-gcc
    make -j"$(nproc)" all-target-libgcc
    make install-gcc
    make install-target-libgcc
}

main() {
    local args
    local nuke
    local clean
    local cctc_dir
    local target

    args=$(parse_args "$@")
    read -r nuke clean cctc_dir target <<< "$args"

    clean_src "$nuke" "$clean" "$cctc_dir"
    get_src "$cctc_dir"
    check_deps
    build_src "$cctc_dir" "$target"
    echo -e "\ntoolchain located in $cctc_dir/gcc-<VERSION>"
}

main "$@"
